
using LinearAlgebra

function trans(vec::Array{Float64})
    mat = Matrix{Float64}(I,4,4)
    mat[1, 4] = vec[1]
    mat[2, 4] = vec[2]
    mat[3, 4] = vec[3]
    mat
end

function scale(vec::Array{Float64})
    mat = Matrix{Float64}(I,4,4)
    mat[1,1] = vec[1]
    mat[2,2] = vec[2]
    mat[3,3] = vec[3]
    mat
end

function rotz(theta::Float64)
    mat = Matrix{Float64}(I,4,4)
    mat[1,1] =  cos(theta)
    mat[1,2] = -sin(theta)
    mat[2,1] =  sin(theta)
    mat[2,2] =  cos(theta)
    mat
end

function rotx(theta::Float64)
    mat = Matrix{Float64}(I,4,4)
    mat[2,2] =  cos(theta)
    mat[2,3] = -sin(theta)
    mat[3,2] =  sin(theta)
    mat[3,3] =  cos(theta)
    mat
end

function roty(theta::Float64)
    mat = Matrix{Float64}(I,4,4)
    mat[1,1] =  cos(theta)
    mat[1,3] =  sin(theta)
    mat[3,1] = -sin(theta)
    mat[3,3] =  cos(theta)
    mat
end

function hom(vec::Array{Float64})
   [vec ; [1]]
end

function deg2rad(deg::Float64)
    (deg * pi) / 360.0
end

function rad2deg(rad::Float64)
    (rad * 360.0) / pi
end

function find_model_midpoint(solid::Solid)
    N::Float64 = size(solid.triangles)[1] * 3.0
    xsum = 0.0
    ysum = 0.0
    zsum = 0.0
    for triangle in solid.triangles
        p0 = triangle.p0
        p1 = triangle.p1
        p2 = triangle.p2
        xsum += (p0[1] + p1[1] + p2[1])
        ysum += (p0[2] + p1[2] + p2[2])
        zsum += (p0[3] + p1[3] + p2[3])
    end
    [xsum/N, ysum/N, zsum/N]
end

function transform_solid(solid::Solid, m::Array{Float64,2})
    zerovec = (m * [0.0, 0.0, 0.0, 1.0])[1:3]
    newsolid = Solid([])
    for triangle in solid.triangles
        p0 = (m * hom(triangle.p0))[1:3]
        p1 = (m * hom(triangle.p1))[1:3]
        p2 = (m * hom(triangle.p2))[1:3]
        #Transformation below does not work when there is both translation and rotation
        newnormal =  (m*hom(triangle.normal - zerovec))[1:3] 
        newtriangle = Triangle(p0, p1, p2, norm(newnormal)*newnormal)
        push!(newsolid.triangles, newtriangle)
    end
    newsolid
end

