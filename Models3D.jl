
module Models3D
include("types.jl")
include("modelio.jl")
include("bitmapify.jl")
include("transforms.jl")

export make_2d_grid_orto,  perspective_to_orto, solid_dimensions,
    trans, scale, rotx, roty, rotz, hom, find_model_midpoint, transform_solid,
    read_model, write_model

end
