

## Reading the model file
#using PyCall
#@pyimport stl

function pytuple_to_vec(pytuple)
    arr = zeros(3)
    for i = 1:3
        arr[i] = pytuple[i]
    end
    arr
end

function from_py_facet(pyfacet)
    normal = pytuple_to_vec(pyfacet[:normal])
    pyvertices = pyfacet[:vertices]
    p1 = pytuple_to_vec(pyvertices[1])
    p2 = pytuple_to_vec(pyvertices[2])
    p3 = pytuple_to_vec(pyvertices[3])
    Triangle(p1,p2,p3,normal)
end

function from_pymodel(pymodel)
    facets = pymodel[:facets]
    numfacets = length(facets)
    triangles = Array{Triangle}(0)
    for i = 1:numfacets
        facet = from_py_facet(facets[i])
        push!(triangles, facet)
    end
    Solid(triangles)
end

function read_model_ascii(path)
    #Read using python-stl
    expr = "open(\"" * path * "\", \"r\")"
    filehandle = pyeval(expr)
    model = from_pymodel(stl.read_ascii_file(filehandle))
    filehandle[:close]()
    model
end

function read_facet(f)
    v = zeros(Float32, 12)
    read!(f, v)
    extrabytes = read(f, UInt16)
    if extrabytes > 0
        throwaway = zeros(UInt8, extrabytes)
        read!(f, throwaway)
    end
    Triangle(v[4:6], v[7:9], v[10:12], v[1:3])
end

function is_ascii_stl(header)    
    solid = [0x73, 0x6f, 0x6c, 0x69, 0x64]
    whitespace = [0x09, 0x0A, 0x0D, 0x20]
    slen = length(solid)
    for i in 1:length(header) - slen
        eq(a) = a==header[i]
        if header[i:slen+i-1] == solid
            return true
        elseif header[i] in whitespace
            continue
        else
            return false
        end
    end
    return false
end

function read_model(path, binary=true)
    f = open(path)
    startvec = zeros(UInt8, 80)
    read!(f, startvec)
    if is_ascii_stl(startvec)
        close(f)
        return read_model_ascii(path)
    end
    num_facets = read(f, UInt32)
    facets = Array{Triangle,1}(undef,0)
    for i in 1:num_facets
        facet = read_facet(f)
        push!(facets, facet)
    end
    close(f)
    Solid(facets)
end

function write_stl_vec(f, txt, vec)
    write(f, txt)
    for el in vec
        write(f, " " * string(el))
    end
    write(f, "\n")
end

function write_model_ascii(model, path, name)
    f = open(path, "w")
    write(f, "solid " * name * "\n")
    for triangle in model.triangles
        write(f, " facet ")
        write_stl_vec(f, "normal", triangle.normal)
        write(f, "    outer loop\n")
        write_stl_vec(f,"      vertex", triangle.p0)
        write_stl_vec(f,"      vertex", triangle.p1)
        write_stl_vec(f,"      vertex", triangle.p2)
        write(f, "    endloop\n")
        write(f, "  endfacet\n")
    end
    write(f, "endsolid " * name * "\n")
    close(f)
end

function write_model_binary(model, path, name)
    
end

function write_model(model, path, binary=true, name="Solid")
    if binary
        write_model_binary(model, path, name)
    else
        write_model_ascii(model, path, name)
    end
end
