
include("Models3D.jl")

import JSON

function read_json_file(path)
    open(path) do f
        content = read(f, String)
        JSON.parse(content)
    end
end

function make_heightmap(args)
    settings = read_json_file(args[1])
    solid = Models3D.read_model(settings["infile"])
    

    xdim, ydim, zdim = Models3D.solid_dimensions(solid)
    res = settings["resolution"]
    dim = Integer(ceil(ydim / res))
    println("dim = ", [ydim, res, dim])
    Models3D.make_2d_grid_orto(solid, dim)
end


#make_toolpath(ARGS)
