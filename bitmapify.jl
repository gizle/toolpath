

function get_points(model::Solid)
    points = Array{Array{Float64,1},1}()
    for triangle in model.triangles
        push!(points, triangle.p0)
        push!(points, triangle.p1)
        push!(points, triangle.p2)
    end
    points
end

function getx(vec::Array{Float64,1})
    vec[1]
end

function gety(vec::Array{Float64,1})
    vec[2]
end

function getz(vec::Array{Float64,1})
    vec[3]
end


abstract type QuadTree
end

struct QuadTreeLeaf <: QuadTree
    startx::Float64
    endx::Float64
    starty::Float64
    endy::Float64
    triangles::Array{Triangle,1}
end

struct QuadTreeInterior <: QuadTree
    startx::Float64
    endx::Float64
    starty::Float64
    endy::Float64
    triangles::Array{Triangle,1}
    upper_left::QuadTree
    upper_right::QuadTree
    lower_left::QuadTree
    lower_right::QuadTree
end

function make_quadtree(triangles::Array{Triangle,1},
                       startx::Float64, endx::Float64,
                       starty::Float64, endy::Float64)
    
    if size(triangles)[1] <= 100
        return QuadTreeLeaf(startx, endx, starty, endy, triangles)
    end

    halfx = (endx + startx) / 2.0
    halfy = (endy + starty) / 2.0
    ul = Array{Triangle,1}(undef,0)
    ur = Array{Triangle,1}(undef,0)
    ll = Array{Triangle,1}(undef,0)
    lr = Array{Triangle,1}(undef,0)
    crossing = Array{Triangle,1}(undef,0)

    for triangle in triangles

        p0 = triangle.p0
        p1 = triangle.p1
        p2 = triangle.p2

        maxx = max(p0[1], p1[1], p2[1])
        minx = min(p0[1], p1[1], p2[1])
        maxy = max(p0[2], p1[2], p2[2])
        miny = min(p0[2], p1[2], p2[2])

        if maxx < halfx && maxy < halfy #LL
            push!(ll,triangle)
        elseif minx > halfx && maxy < halfy #LR
            push!(lr,triangle)
        elseif maxx < halfx && miny > halfy #UL
            push!(ul, triangle)
        elseif minx > halfx && miny > halfy #UR
            push!(ur, triangle)
        else
            push!(crossing, triangle) #Crossing quadrants
        end
    end

    qul = make_quadtree(ul, startx, halfx, halfy,  endy)
    qur = make_quadtree(ur, halfx,  endx,  halfy,  endy)
    qll = make_quadtree(ll, startx, halfx, starty, halfy)
    qlr = make_quadtree(lr, halfx,  endx,  starty, halfy)

    return QuadTreeInterior(startx, endx, starty, endy, crossing,
                            qul,qur,qll,qlr)
end

function make_quadtree(model::Solid)
    points = get_points(model)
    xvals = map(getx, points)
    yvals = map(gety, points)
    zvals = map(getz, points)
    maxx = maximum(xvals)
    minx = minimum(xvals)
    maxy = maximum(yvals)
    miny = minimum(yvals)
    return make_quadtree(model.triangles, minx, maxx, miny, maxy)
end

function intersection_distance(triangles::Array{Triangle,1}, ray::Ray)
    #Möller and Trumbore's algorithm for triangle intersection distance.
    #http://www.graphics.cornell.edu/pubs/1997/MT97.pdf
    epsilon::Float64 = 0.00000001
    distance::Float64 = Inf
    dir::Array{Float64,1} = ray.direction
    orig::Array{Float64,1} = ray.origin
    for i = 1:length(triangles)
        triangle = triangles[i]
        vert0::Array{Float64,1} = triangle.p0
        vert1::Array{Float64,1} = triangle.p1
        vert2::Array{Float64,1} = triangle.p2
        edge1::Array{Float64,1} = vert1 - vert0
        edge2::Array{Float64,1} = vert2 - vert0
        p::Array{Float64,1} = cross(dir, edge2)
        det::Float64 = dot(edge1, p)

        if det > -epsilon && det < epsilon
            continue
        end

        inv_det::Float64 = 1.0 / det
        t::Array{Float64,1} = orig - vert0
        u::Float64 = dot(t, p) * inv_det
        if u < 0.0 || u > 1.0
            continue
        end

        q::Array{Float64,1} = cross(t, edge1)
        v::Float64 = dot(dir, q) * inv_det
        if v < 0.0 || v + u > 1.0
            continue
        end

        distance = min(dot(edge2, q) * inv_det, distance)
    end
    distance
end

function intersection_distance(qt::QuadTreeLeaf, ray::Ray)
    return intersection_distance(qt.triangles, ray)
end

function intersection_distance(qt::QuadTreeInterior, ray::Ray)
    dist1 = intersection_distance(qt.triangles, ray)
    dist2 = Inf

    if ray.origin[1] < qt.upper_left.endx && ray.origin[2] > qt.upper_left.starty 
        dist2 = intersection_distance(qt.upper_left, ray)

    elseif ray.origin[1] > qt.upper_right.startx && ray.origin[2] > qt.upper_right.starty 
        dist2 = intersection_distance(qt.upper_right, ray)

    elseif ray.origin[1] < qt.lower_left.endx && ray.origin[2] < qt.lower_left.endy
        dist2 = intersection_distance(qt.lower_left, ray)

    elseif ray.origin[1] > qt.lower_right.startx && ray.origin[2] < qt.lower_right.endy
        dist2 = intersection_distance(qt.lower_right, ray)
    end

    return min(dist1, dist2)
end

function compute_normal(p0, p1, p2, normal)
    vec1 = p1 - p0
    vec2 = p2 - p0
    newnormal = cross(vec1, vec2)
    if dot(newnormal, normal) < 0.0
        newnormal = -newnormal
    end
    newnormal
end

function perspective_to_orto(point::Array{Float64}, zview::Float64)
    viewpoint = [0, 0, zview]
    z0 = zview - point[3]    
    x1 = point[1] * z0
    y1 = point[2] * z0
    [x1, y1, point[3]]
end

function perspective_to_orto(solid::Solid, distance::Float64)
    new_solid = Solid([])
    for triangle in solid.triangles
        p0 = triangle.p0
        p1 = triangle.p1
        p2 = triangle.p2
        normal = triangle.normal

        new_p0 = perspective_to_orto(p0, distance)
        new_p1 = perspective_to_orto(p1, distance)
        new_p2 = perspective_to_orto(p2, distance)
        new_normal = compute_normal(new_p0, new_p1, new_p2, normal)
        
        new_triangle = Triangle(new_p0, new_p1, new_p2, new_normal)
        push!(new_solid.triangles, new_triangle)
    end
    new_solid
end

function cull_model(solid::Solid, dir)
    new_solid = Solid([])
    for triangle in solid.triangles
        if dot(dir, triangle.normal) < 0.0
            push!(new_solid.triangles, triangle)
        end
    end
    new_solid
end

function solid_dimensions(model::Solid)
    points = get_points(model)
    xvals = map(getx, points)
    yvals = map(gety, points)
    zvals = map(getz, points)
    maxx = maximum(xvals)
    minx = minimum(xvals)
    maxy = maximum(yvals)
    miny = minimum(yvals)
    maxz = maximum(zvals)
    minz = minimum(zvals)

    xrange = Float64(maxx - minx)
    yrange = Float64(maxy - miny)
    zrange = Float64(maxz - minz)
    xrange, yrange, zrange
end


function make_2d_grid_orto(model::Solid, ydim::Integer)
    quadtree = make_quadtree(model::Solid)
    
    points = get_points(model)
    xvals = map(getx, points)
    yvals = map(gety, points)
    zvals = map(getz, points)
    maxx = maximum(xvals)
    minx = minimum(xvals)
    maxy = maximum(yvals)
    miny = minimum(yvals)
    maxz = maximum(zvals)
    minz = minimum(zvals)

    xrange = Float64((maxx - minx)*1.05)
    yrange = Float64((maxy - miny)*1.05)
    zrange = Float64(maxz - minz)

    print([xrange, yrange, zrange])
    
    step = yrange / ydim
    zstep = 1.0 / zrange
    xdim =  Int32(ceil(xrange/yrange * ydim))

    arr = ones(xdim, ydim) .* minz
    for j = 1:ydim
        println("line " * string(j) * " of " * string(ydim))
        for i = 1:xdim
            x = maxx - i*step + xrange*0.025
            y = miny + j*step - yrange*0.025
            pos = [x, y, maxz]
            dir = [0.0,0.0,-1.0]
            ray = Ray(pos, dir)
            dist = intersection_distance(quadtree, ray)
            if dist > maxz-minz
                arr[i,j] = 0.0
            else
                arr[i,j] = (maxz - dist - minz) * zstep
            end
        end
    end
    arr, xrange, yrange, zrange
end
