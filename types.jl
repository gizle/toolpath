
struct Triangle
    p0::Array{Float64,1}
    p1::Array{Float64,1}
    p2::Array{Float64,1}
    normal::Array{Float64,1}
end

struct Solid
    triangles::Array{Triangle,1}
end

struct Ray
    origin::Array{Float64,1}
    direction::Array{Float64,1}
end
